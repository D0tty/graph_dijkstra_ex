# -*- coding: utf-8 -*-
"""
Created on Mon May 15 22:21:51 2017

@author: Thomas
"""

from graph import Graph
from graph import GraphNode
from graph import GraphLink


a = GraphNode('A')
b = GraphNode('B')
c = GraphNode('A')
d = GraphNode('D')
e = GraphNode('E')

a.succ.append(GraphLink(4, d))
a.succ.append(GraphLink(2, e))
a.succ.append(GraphLink(10, b))

b.succ.append(GraphLink(10, a))
b.succ.append(GraphLink(3, e))
b.succ.append(GraphLink(7, c))

c.succ.append(GraphLink(7, b))
c.succ.append(GraphLink(8, d))

d.succ.append(GraphLink(8, c))
d.succ.append(GraphLink(1, e))
d.succ.append(GraphLink(4, a))

e.succ.append(GraphLink(1, d))
e.succ.append(GraphLink(2, a))
e.succ.append(GraphLink(3, b))

g = Graph([a, b, c, d, e])