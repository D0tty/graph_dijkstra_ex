# -*- coding: utf-8 -*-
"""
Created on Mon May 15 22:18:24 2017

@author: Thomas
"""
import rec
import lvlup

def costfromNtoX(n, x):
    for s in x.succ:
        if ( s.dst == n ):
            return s.cost
    return 999999

def dijkstra(g, start, end):
    closedset = []
    openset = [n for n in g.nodes]
    for n in openset:
        n.gval = 999999
        n.prev = 0
    start.gval = 0
    while not ( end in closedset):
        x = min( openset, key=lambda x: x.gval)
        openset.remove(x)
        closedset.append(x)
        if not ( x == end ):
            for n in list(set([f.dst for f in x.succ]).union(openset)):
                gp = x.gval + costfromNtoX(n, x)
                if ( gp < n.gval ):
                    n.gval = gp
                    n.prev = x
                    
def printPath(end):
    pathl = [end]
    while(end.prev):
        pathl.append(end.prev)
        end = end.prev
    path = ""
    for i in range(len(pathl) -1 , -1, -1):
        if not ( i == 0 ):
            path += str(pathl[i].name) + " -> "
        else:
            path += str(pathl[i].name)
    print(path)
    return path


#rec test
start = rec.a
end = rec.b
dijkstra(rec.g, start, end)
path = printPath(end)

#lvlup test
start = lvlup.a
end = lvlup.e
dijkstra(lvlup.g, start, end)
path = printPath(end)