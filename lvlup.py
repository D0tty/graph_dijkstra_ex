# -*- coding: utf-8 -*-
"""
Created on Mon May 15 23:19:08 2017

@author: Thomas
"""

from graph import Graph
from graph import GraphNode
from graph import GraphLink


a = GraphNode('A')
b = GraphNode('B')
c = GraphNode('A')
d = GraphNode('D')
e = GraphNode('E')
f = GraphNode('F')
g = GraphNode('G')

a.succ.append(GraphLink(20, g))
a.succ.append(GraphLink(10, b))

b.succ.append(GraphLink(10, a))
b.succ.append(GraphLink(3, c))

c.succ.append(GraphLink(3, b))
c.succ.append(GraphLink(30, d))

d.succ.append(GraphLink(30, c))
d.succ.append(GraphLink(3, g))

g.succ.append(GraphLink(3, d))
g.succ.append(GraphLink(5, f))

f.succ.append(GraphLink(5, g))
f.succ.append(GraphLink(5, e))

e.succ.append(GraphLink(5, f))

g = Graph([a, b, c, d, e, f, g])