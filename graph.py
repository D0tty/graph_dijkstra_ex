# -*- coding: utf-8 -*-
"""
Created on Mon May 15 22:18:24 2017

@author: Thomas
"""

class Graph:
    def __init__(self, nodes):
        self.nodes = nodes
        
class GraphNode:
    def __init__(self, name):
        self.name = name
        self.succ = []
        self.gval = 0
        self.prev = None

class GraphLink:
    def __init__(self, cost, dest):
        self.cost = cost
        self.dst = dest